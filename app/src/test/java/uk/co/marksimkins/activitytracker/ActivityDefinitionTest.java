package uk.co.marksimkins.activitytracker;

import uk.co.marksimkins.activitytracker.model.ActivityDefinition;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricTestRunner.class)
public class ActivityDefinitionTest {

    @Test
    public void testIsValidNull() {
        ActivityDefinition definition = new ActivityDefinition(null);
        assertEquals(ActivityDefinition.Validity.BLANK, definition.isValid());
    }

    @Test
    public void testIsValidEmpty() {
        ActivityDefinition definition = new ActivityDefinition("");
        assertEquals(ActivityDefinition.Validity.BLANK, definition.isValid());
    }

    @Test
    public void testIsValidEntry() {
        ActivityDefinition definition = new ActivityDefinition("valid entry");
        assertEquals(ActivityDefinition.Validity.VALID, definition.isValid());
    }

    @Test
    public void testIsValidTooLong() {
        ActivityDefinition definition = new ActivityDefinition("invalid entry that is too long!");
        assertEquals(ActivityDefinition.Validity.TOO_LONG, definition.isValid());
    }
}