package uk.co.marksimkins.activitytracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import uk.co.marksimkins.activitytracker.model.ActivityDefinition;

import java.util.List;

/**
 * An extension of ArrayAdapter, providing Activity-specific features such as lazy loading, last-incremented
 * highlighting, incrementing button count, and general rendering goodness.
 */
public class ActivityDefAdapter extends ArrayAdapter<ActivityDefinition> {

    private static final int NO_LAST_INCREMENTED = -1;
    private static final int LAST_INCREMENTED_COLOUR = Color.rgb(240, 240, 255);

    private int lastIncrementedPosition = -1;

    public ActivityDefAdapter(Context context, List<ActivityDefinition> defs) {
        super(context, 0, defs);
        lastIncrementedPosition = loadLastIncrementedPosition();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final ActivityDefinition def = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_item, parent, false);
        }

        // If this position happens to be the last incremented activity, highlight it
        if (lastIncrementedPosition == position) {
            convertView.setBackgroundColor(LAST_INCREMENTED_COLOUR);
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        Button button = convertView.findViewById(R.id.counter_button);
        button.setOnClickListener(new ActivityOnClickListener(def, convertView, position));

        final TextView activityName = convertView.findViewById(R.id.activityName);
        activityName.setText(def.getName());

        setButtonCount(button, def.getCount().getCount());
        return convertView;
    }

    /**
     * Customise the increment button text with the current count.
     *
     * @param button The button to set the text on.
     * @param count  What count to set on the button.
     */
    private void setButtonCount(Button button, int count) {
        button.setText(count + "++");
    }

    /**
     * Load the last incremented activity position from shared preferences.
     *
     * @return The position of the last incremented activity, if any.
     */
    private int loadLastIncrementedPosition() {
        Context context = getContext();
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_prefs), Context.MODE_PRIVATE);
        return sharedPref.getInt(context.getString(R.string.last_incremented_key), NO_LAST_INCREMENTED);
    }


    /**
     * OnClickListener for the activity increment buttons.
     */
    private class ActivityOnClickListener implements View.OnClickListener {

        private final ActivityDefinition def;
        private final View convertView;
        private final int position;

        ActivityOnClickListener(ActivityDefinition def, View convertView, int position) {
            this.def = def;
            this.convertView = convertView;
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            def.getCount().incrementCount();
            def.saveAll();
            setButtonCount((Button) view, def.getCount().getCount());
            setLastIncremented();
        }

        /**
         * Sets the position of the last incremented activity to this position, and ensure
         * appropriate highlighting.
         */
        private void setLastIncremented() {
            if (lastIncrementedPosition != NO_LAST_INCREMENTED) {
                ((ListView) convertView.getParent()).getChildAt(lastIncrementedPosition).setBackgroundColor(Color.TRANSPARENT);
            }
            convertView.setBackgroundColor(LAST_INCREMENTED_COLOUR);
            lastIncrementedPosition = position;
            saveLastIncremented(lastIncrementedPosition);
        }

        /**
         * Save the last incremented position to the shared preferences store, so that it can be
         * retrieved on app reboot.
         * @param position The position to save.
         */
        private void saveLastIncremented(int position) {
            Context context = getContext();
            SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_prefs), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(context.getString(R.string.last_incremented_key), position);
            editor.commit();
        }
    }
}
