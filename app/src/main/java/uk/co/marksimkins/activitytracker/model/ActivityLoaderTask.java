package uk.co.marksimkins.activitytracker.model;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import uk.co.marksimkins.activitytracker.ActivityDefAdapter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * AsyncTask to load the activity definitions from the local database, and today's counts for these
 * definitions. It will then update the ActivityDefAdapter with the loaded data when it's done.
 */
public class ActivityLoaderTask extends AsyncTask<Void, Void, List<ActivityDefinition>> {

    private Context context;
    private View rootView;

    public ActivityLoaderTask(Context context, View rootView) {
        this.context = context;
        this.rootView = rootView;
    }

    @Override
    protected List<ActivityDefinition> doInBackground(Void... params) {
        List<ActivityDefinition> allDefs = ActivityDefinition.selectAll();
        List<ActivityCount> allTodaysCounts = ActivityCount.selectTodays();

        for (ActivityDefinition def : allDefs) {
            Iterator<ActivityCount> it = allTodaysCounts.iterator();
            def.setCount(new ActivityCount(def.getId())); //set default count on the relevant definition

            while (it.hasNext()) {
                ActivityCount count = it.next();

                if (count.getDefinitionId() == def.getId()) {
                    // count and def match - update def's count, then remove the count entry so it's
                    // not processed again in this loader.
                    def.setCount(count);
                    it.remove();
                    break;
                }
            }
        }
        return allDefs;
    }

    @Override
    protected void onPostExecute(List<ActivityDefinition> activityDefinitions) {

        if (activityDefinitions.size() > 0) {
            // Remove the default text, if it's still there
            View view = rootView.findViewById(uk.co.marksimkins.activitytracker.R.id.default_activity_text);
            if (view != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
            // Add the items to the listview
            ActivityDefAdapter adapter = new ActivityDefAdapter(context, new ArrayList<ActivityDefinition>());
            adapter.addAll(activityDefinitions);
            ListView listView = rootView.findViewById(uk.co.marksimkins.activitytracker.R.id.activity_list);
            listView.setAdapter(adapter);
        }
    }
}
