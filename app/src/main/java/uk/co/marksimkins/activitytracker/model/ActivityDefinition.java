package uk.co.marksimkins.activitytracker.model;

import com.orm.SugarRecord;
import com.orm.query.Select;

import java.util.List;

/**
 * This defines an activity. Quite simply, it's a name, with the count linked to it at runtime.
 */
public class ActivityDefinition extends SugarRecord {

    public static final int MAX_NAME_LENGTH = 30;

    /** Various validation states for adding a new activity definition. */
    public enum Validity {
        VALID,
        BLANK,
        TOO_LONG
    }

    /** The name of the activity. */
    private String name;

    /**
     * How many times the activity has been done in a given day.
     * Only ever set within ActivityLoaderTask.
     */
    private ActivityCount count;

    // Annoyingly, needed for Sugar ORM.
    public ActivityDefinition() {}

    /**
     * Create a brand new definition for saving to the database.
     *
     * @param name The new name.
     */
    public ActivityDefinition(String name) {
        this.name = name;
    }

    /**
     * @return The name of this activity.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the activity count on this definition.
     *
     * @param count How often this activity has been done in a given day.
     */
    public void setCount(ActivityCount count) {
        this.count = count;
    }

    /**
     * @return The number of times this activity has been done in a given day.
     */
    public ActivityCount getCount() {
        if (count == null) {
            count = new ActivityCount(this.getId());
        }
        return count;
    }

    /**
     * @return The Validity of this activity definition.
     */
    public Validity isValid() {
        if (name == null || name.isEmpty()) {
            return Validity.BLANK;
        }
        if (name.length() > MAX_NAME_LENGTH) {
            return Validity.TOO_LONG;
        }
        return Validity.VALID;
    }

    /**
     * @return Convenience method to load all activity definitions from the database.
     */
    public static List<ActivityDefinition> selectAll() {
        return Select.from(ActivityDefinition.class).orderBy("name COLLATE NOCASE ASC").list();
    }

    /**
     * Convenience method to save any changes to this definition, and any counts related to it.
     */
    public void saveAll() {
        this.save();
        count.save();
    }

}
