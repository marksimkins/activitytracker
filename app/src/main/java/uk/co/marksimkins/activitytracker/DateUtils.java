package uk.co.marksimkins.activitytracker;

import org.joda.time.LocalDate;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A little collection of useful Date-related utilities.
 */
public class DateUtils {

    /** A universal date format. */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEE dd MMM, yyyy");

    /**
     * @return A SQL Date object for today.
     */
    public static Date getTodayInSqlDate() {
        return new Date(LocalDate.now().toDateTimeAtStartOfDay().getMillis());
    }

    /**
     * @return A Calendar object for today.
     */
    public static Calendar getTodayAsCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(LocalDate.now().toDateTimeAtStartOfDay().getMillis());
        return cal;
    }

    /**
     * Given numeric values for the date, return the millisecond value for the start of that day.
     *
     * @param year  Numeric year.
     * @param month Numeric month of year. 1-based.
     * @param day   Numeric day of month.
     * @return  Millisecond value for the start of the specified day.
     */
    public static long getDateAtStartAsMillis(int year, int month, int day) {
        return new LocalDate(year, month, day).toDate().getTime();
    }

    /**
     * @return Get today's date rendered as a nice string.
     */
    public static String getTodayString() {
        return formatDate(LocalDate.now().toDate().getTime());
    }

    /**
     * Format a given millisecond value into a nicely rendered date string.
     *
     * @param millis The millisecond value to convert to a date string.
     * @return  A nicely formatted date string.
     */
    public static String formatDate(long millis) {
        return DATE_FORMAT.format(millis);
    }

}
