package uk.co.marksimkins.activitytracker;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Main Activity, holding the Fragments for the different parts of the app.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Add the default ActivityListFragment on first load
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.flContent, new ActivityListFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment;
        Class fragmentClass;
        switch(item.getItemId()) {
            case R.id.nav_activities:
                fragmentClass = ActivityListFragment.class;
                break;
            case R.id.nav_reporting:
                fragmentClass = ReportingFragment.class;
                break;
            default:
                fragmentClass = ActivityListFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            Log.e(TAG, "onNavigationItemSelected: cannot get fragment, cannot make the change", e);
            Toast.makeText(this, "Sorry, an error has occurred", Toast.LENGTH_SHORT).show();
            return false;
        }

        final ActionBar actionBar = getSupportActionBar();

        // Insert the fragment by replacing any existing fragment
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
        // Lambdas would be nice.
        fragmentManager.addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        Fragment f = fragmentManager.findFragmentById(R.id.flContent);
                        NavigationView nav = findViewById(R.id.nav_view);
                        if (f instanceof ActivityListFragment) {
                            if (actionBar != null) {
                                actionBar.setTitle(R.string.activity_tracker);
                            }
                            nav.getMenu().getItem(0).setChecked(true);
                        } else if (f instanceof ReportingFragment) {
                            if (actionBar != null) {
                                actionBar.setTitle(R.string.title_activity_reporting);
                            }
                            nav.getMenu().getItem(1).setChecked(true);
                        }
                    }
                });

        // Highlight the selected item
        item.setChecked(true);
        // Set action bar title
        if (actionBar != null) {
            actionBar.setTitle(item.getTitle());
        }
        // Close the navigation drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
