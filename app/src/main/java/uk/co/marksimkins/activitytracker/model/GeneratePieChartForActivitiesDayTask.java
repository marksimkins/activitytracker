package uk.co.marksimkins.activitytracker.model;

import android.os.AsyncTask;
import android.view.View;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import uk.co.marksimkins.activitytracker.DateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Load the dataset for a selected day's activities for graphing.
 */
public class GeneratePieChartForActivitiesDayTask extends AsyncTask<Void, Void, Void> {

    private View rootView;
    private long dayMillis;

    private Map<String, Entry> activityData;

    private PieChart pieChart;
    private PieDataSet pieDataSet;
    private PieData pieData;

    /**
     * Generate the pie chart for the given day's activities.
     *
     * @param rootView  The view to render the pie chart.
     * @param dayMillis Which day's activities to retrieve.
     */
    public GeneratePieChartForActivitiesDayTask(View rootView, long dayMillis) {
        this.rootView = rootView;
        this.dayMillis = dayMillis;
        if (dayMillis == 0) {
            this.dayMillis = DateUtils.getTodayInSqlDate().getTime();
        }
    }

    @Override
    protected void onPreExecute() {
        pieChart = rootView.findViewById(uk.co.marksimkins.activitytracker.R.id.chart);
        pieChart.setNoDataText(rootView.getContext().getString(uk.co.marksimkins.activitytracker.R.string.reporting_loading));
    }

    @Override
    protected Void doInBackground(Void... params) {
        List<ActivityDefinition> allDefs = ActivityDefinition.selectAll();
        List<ActivityCount> allCountsForDay = ActivityCount.selectDay(dayMillis);

        // Time to initialise all the data needed for the pie chart.
        activityData = new HashMap<>();
        int numEntries = 0;
        for (ActivityCount count : allCountsForDay) {
            Iterator<ActivityDefinition> it = allDefs.iterator();
            while (it.hasNext()) {
                ActivityDefinition def = it.next();
                if (def.getId() == count.getDefinitionId()) {
                    activityData.put(def.getName(), new Entry(count.getCount(), numEntries++));
                    it.remove();
                    break;
                }
            }
        }

        pieDataSet = new PieDataSet(new ArrayList<>(activityData.values()), "");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieData = new PieData(new ArrayList<>(activityData.keySet()), pieDataSet);
        pieData.setValueTextSize(12);
        pieData.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return "(" + (int) value + ")";
            }
        });
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        // Now we render the pie chart with the loaded data.
        pieChart.setData(pieData);
        pieChart.setDescription(""); // Don't want a description.
        pieChart.setTouchEnabled(false);
        Legend legend = pieChart.getLegend();
        legend.setWordWrapEnabled(true);
        legend.setTextSize(15);
        legend.setYOffset(legend.getYOffset() + 1);
        legend.setXEntrySpace(legend.getXEntrySpace() + 50); // Needs doing properly, maybe in a vertical list
        pieChart.invalidate();
    }
}
