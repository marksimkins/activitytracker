package uk.co.marksimkins.activitytracker;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * The root Application class. This is only really here so that I can initialise JodaTimeAndroid,
 * and extending SugarApp so I can make use of Sugar ORM.
 */
public class Application extends com.orm.SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }

}
