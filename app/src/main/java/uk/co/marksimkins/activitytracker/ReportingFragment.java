package uk.co.marksimkins.activitytracker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import uk.co.marksimkins.activitytracker.model.GeneratePieChartForActivitiesDayTask;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass, showing the reporting of the activities and the proportion of time
 * spent on each of them.
 */
public class ReportingFragment extends Fragment {

    private static int selectedYear;
    private static int selectedMonth;
    private static int selectedDay;

    private static long selectedMillis;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View createdView = inflater.inflate(R.layout.fragment_reporting, container, false);

        loadReport(createdView);

        return createdView;
    }

    /**
     * Load the selected report into the provided view.
     * @param view  The view into which we load the report.
     */
    private void loadReport(View view) {
        Log.i("ReportingFragment", "Loading report for millis " + selectedMillis);
        GeneratePieChartForActivitiesDayTask pieChartGenerator = new GeneratePieChartForActivitiesDayTask(view, selectedMillis);
        pieChartGenerator.execute();

        final TextView reportDate = view.findViewById(R.id.report_date);
        if (selectedMillis == 0) {
            reportDate.setText(DateUtils.getTodayString());
        } else {
            reportDate.setText(DateUtils.formatDate(selectedMillis));
        }

        Button changeDateBtn = view.findViewById(R.id.btn_change_report_date);
        changeDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDateFragment newFragment = new SelectDateFragment();
                newFragment.setReportDateTextView(reportDate);
                newFragment.setHandler(new DialogHandler());
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });
    }

    /**
     * Upon closing the date picker dialog, we need to re-load the report.
     */
    class DialogHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            loadReport(getView());
        }

    }

    /**
     * The date picker dialog, for changing which date of activity data to display.
     */
    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        private TextView reportDate;
        private Handler handler;

        /**
         * Set the textview that will render the date selected.
         * @param reportDate The textview that will render the date selected.
         */
        public void setReportDateTextView(TextView reportDate) {
            this.reportDate = reportDate;
        }

        /**
         * Set the handler that will be called when this dialog is closed.
         * @param handler Call this when you're done.
         */
        public void setHandler(Handler handler) {
            this.handler = handler;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (selectedMillis == 0) {
                final Calendar calendar = DateUtils.getTodayAsCalendar();
                selectedYear = calendar.get(Calendar.YEAR);
                selectedMonth = calendar.get(Calendar.MONTH);
                selectedDay = calendar.get(Calendar.DAY_OF_MONTH);
                selectedMillis = calendar.getTimeInMillis();
            }
            DatePickerDialog dpd = new DatePickerDialog(getActivity(), this, selectedYear, selectedMonth, selectedDay);
            dpd.getDatePicker().setFirstDayOfWeek(Calendar.MONDAY);
            return dpd;
        }

        @Override
        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            selectedYear = yy;
            selectedMonth = mm;
            selectedDay = dd;

            //jodatime is one-based
            selectedMillis = DateUtils.getDateAtStartAsMillis(yy, mm + 1, dd);
            reportDate.setText(DateUtils.formatDate(selectedMillis));

            // Now tell the fragment to reload
            handler.sendEmptyMessage(0);
        }
    }
}
