package uk.co.marksimkins.activitytracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.marksimkins.activitytracker.model.ActivityDefinition;
import uk.co.marksimkins.activitytracker.model.ActivityLoaderTask;


/**
 * A simple {@link Fragment} subclass, to display the list of Activities created, and their counts for the day.
 */
public class ActivityListFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View createdView = inflater.inflate(R.layout.fragment_activity_list, container, false);

        // Set up the lovely floating action button at the bottom right.
        // I'm so looking forward to lambdas being widely supported..!
        FloatingActionButton fab = createdView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_TEXT);

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.alert_add_activity);
                builder.setView(input);
                builder.setPositiveButton(R.string.alert_save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {} // This is overridden below.
                });
                builder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                // Now override the save button handler to not close the dialog if there's a validation issue.
                Button saveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                saveButton.setOnClickListener(new AddActivityButtonHandler(dialog, input));
            }
        });

        TextView dateMessage = createdView.findViewById(R.id.current_date_text);
        dateMessage.setText(String.format(getString(R.string.counts_for_today), DateUtils.getTodayString()));

        loadActivities(createdView);
        return createdView;
    }

    /**
     * Kick off the task to load the activities in a background thread.
     *
     * @param rootView The rootview to receive the loaded activities.
     */
    private void loadActivities(View rootView) {
        ActivityLoaderTask loader = new ActivityLoaderTask(getContext(), rootView);
        loader.execute();
    }

    /**
     * Save a new activity.
     *
     * @param name The name given to the activity.
     * @return Whether the name passed validation and was successfully saved.
     */
    private boolean saveAddition(String name) {
        ActivityDefinition activityDefinition = new ActivityDefinition(name);
        switch (activityDefinition.isValid()) {
            case BLANK: {
                Toast toast = Toast.makeText(getContext(), R.string.activity_validation_no_name, Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }
            case TOO_LONG: {
                Toast toast = Toast.makeText(getContext(),
                        String.format(getString(R.string.activity_validation_name_too_long), ActivityDefinition.MAX_NAME_LENGTH),
                        Toast.LENGTH_SHORT);
                toast.show();
                return false;
            }
            case VALID: {
                activityDefinition.save();
                loadActivities(getView());
                return true;
            }
            default:
                Log.w("ActivityListFragment", "saveAddition: Unexpected validity type " + activityDefinition.isValid());
        }
        return false;
    }


    /**
     * Handler for adding an activity, not closing the dialog if there's a validation problem.
     */
    private class AddActivityButtonHandler implements View.OnClickListener {

        private AlertDialog dialog;
        private EditText input;

        AddActivityButtonHandler(AlertDialog dialog, EditText input) {
            this.dialog = dialog;
            this.input = input;
        }

        @Override
        public void onClick(View v) {
            if (saveAddition(input.getText().toString())) {
                dialog.dismiss();
            }
        }
    }
}
