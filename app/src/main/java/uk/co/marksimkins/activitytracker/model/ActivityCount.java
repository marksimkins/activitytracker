package uk.co.marksimkins.activitytracker.model;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;
import uk.co.marksimkins.activitytracker.DateUtils;

import java.util.List;

/**
 * A record of counts against a specific activity definition, logged per day.
 */
public class ActivityCount extends SugarRecord {

    /** The ID of the activity definition this count is related to. */
    private long definitionId;

    /** The millisecond date for this count. Currently daily. */
    private long dateMillis;

    /** How many times this activity has been done. */
    private int count;

    // Needed for ORM
    public ActivityCount() {}

    /**
     * Create a new default count for the provided activity definition id.
     *
     * @param definitionId The ID of the activity definition this count is for.
     */
    public ActivityCount(long definitionId) {
        this.definitionId = definitionId;
        this.dateMillis = DateUtils.getTodayInSqlDate().getTime();
    }

    /**
     * @return The ID of the activity definition this count is for.
     */
    public long getDefinitionId() {
        return definitionId;
    }

    /**
     * @return How many times this activity has been done.
     */
    public int getCount() {
        return count;
    }

    /**
     * Increment the count for this definition. If it's a new day, save what we had, update for the
     * new day, and reset.
     */
    public void incrementCount() {
        long tempMillis = DateUtils.getTodayInSqlDate().getTime();
        if (tempMillis > dateMillis) {
            // It's a new day! Save the current state, then reset
            this.save();
            dateMillis = tempMillis;
            count = 0;
        }
        count++;
    }

    /**
     * Convenience method to select all counts for today.
     *
     * @return A list of ActivityCounts made today.
     */
    public static List<ActivityCount> selectTodays() {
        return selectDay(DateUtils.getTodayInSqlDate().getTime());
    }

    /**
     * Convenience method to select all counts for the specific millisecond value passed in.
     *
     * @param dateMillis The millisecond value for counts.
     * @return A list of ActivityCounts made matching the dateMillis exactly.
     */
    public static List<ActivityCount> selectDay(long dateMillis) {
        return Select.from(ActivityCount.class).where(Condition.prop("DATE_MILLIS").eq(dateMillis)).list();
    }

}
