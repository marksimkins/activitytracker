Track the rough proportion of time you spend doing various activities each day.

It's simple - add a new activity (not using Android terminology!) to briefly describe what you're doing (e.g. "Meetings", "Emails", "Programming", "Watching TV"), and when you start doing the activity, simply tap the "++" button next to it.

You can then view a pie chart showing the proportion of activities throughout the day.

[![alt text](screenshots/1_th.jpg "No activities")](screenshots/1.jpg)
[![alt text](screenshots/2_th.jpg "Activities added")](screenshots/2.jpg)
[![alt text](screenshots/3_th.jpg "Changing to reporting navigation drawer")](screenshots/3.jpg)
[![alt text](screenshots/4_th.jpg "Reporting pie chart on day's activities")](screenshots/4.jpg)

It's by no means fully accurate, but might show some interesting insights. There are more basic features I'd like to add (see below), and it's also not beautified at all - I'm no visual designer! It was also more of a learning experience than an app I intended to completely polish and launch, but who knows, that might happen sometime.

So, my current to do list:

**Reporting Improvements**
* Add week selector
* Add "All time" selector
* Improve layout of reporting legend
* General style improvements, animations

**General Improvements**
* Highlight days with reports in datepicker
* Add swipe to list view, to delete or edit
* Add decrement/undo?
* General style improvements, animations
* Improve unit tests